# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 19:19:02 2013

@author: Pedro Luis
"""

from tkinter import Canvas, PhotoImage, StringVar, filedialog
from tkinter.ttk import Frame, Progressbar, Combobox, Button, Label
from tkinter import FIRST, LAST, YES, BOTH, RAISED, LEFT, TOP, X, Y, BOTTOM
from PIL import Image
import math
import threading


class MainFrame(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.__initComponents()
        # self.__createAxes()
        # self.__drawHistogram()

        self.pack(fill=BOTH, expand=YES)
        #
        self.__loadingImage('qt-logo.jpg')
        
        self.__canvas.bind('<Configure>', self.__resizeCanvas)

    def __initComponents(self):
        # self.__fn = Image.open('qt-logo.jpg')
        # self.__fn = self.__createDrawableImage('python_logo.png');
        # print (self.__fn.getbands())
        # self.__fn.show()
        # bands = self.__fn.getbands()
        # if not(('R' in bands) and ('G' in bands)\
        # and ('B' in bands)):
        # raise ValueError("Image doesn't RGB")
        self.__option = 1
        # tool bar
        self.__toolbar = Frame(self, relief=RAISED, padding=3)
        self.__buttonOpenImageFile = Button(self.__toolbar, command=self.__actionButtonOpenFileImage,
                                            text="Open Image")
        self.__buttonOpenImageFile.pack(side=LEFT)

        self.__lblT = Label(self.__toolbar, text="Option:")    
        self.__lblT.pack(side=LEFT)
        self.__box = StringVar()
        self.__comboboxTypeG = Combobox(self.__toolbar, values=("Line", "Fill"), state='readonly',
                                        textvariable=self.__box)
        self.__comboboxTypeG.current(newindex=self.__option)
        self.__comboboxTypeG.bind("<<ComboboxSelected>>", self.__actionComboboxTypeG)
        self.__comboboxTypeG.pack(side=LEFT, fill=Y)
        
        self.__toolbar.pack(side=TOP, fill=X)
        # content
        self.__canvas = Canvas(self, bg='white', width=600, height=400)
        self.__canvas.pack(fill=BOTH, expand=YES)
        # status bar
        self.__statusbar = Frame(self, relief=RAISED, padding=3)

        self.__lblmsg = Label(self.__statusbar)
        self.__lblmsg.pack(side=LEFT)
        
        self.__progressbar = Progressbar(self.__statusbar)
        self.__progressbar.pack(side=BOTTOM, fill=X)
        
        self.__statusbar.pack(side=BOTTOM, fill=X)
        
        self.__histogram = []

    def __createPhotoImage(self, img):
        """
        Create PhotoImage object

        :param img: Image
        """
        poi = PhotoImage(width=img.size[0], height=img.size[1])
        self.__histogram = [0 for _ in range(256*3)]
        for i in range(img.size[0]):
            for j in range(img.size[1]):
                pixel = img.getpixel((i, j))
                r, g, b = pixel[0], pixel[1], pixel[2]
                poi.put("#%02x%02x%02x" % (r, g, b), (i, j))
                self.__histogram[r] += 1
                self.__histogram[g + 256] += 1
                self.__histogram[b + 512] += 1
                
        del r, g, b, pixel
        return poi

    def __actionButtonOpenFileImage(self): 
        """
        Open a bitmap image:
            *.jpg
            *.jpeg
            *.bmp
        """
        types = [('Images' , '*.jpg *.jpeg *.bmp')]
        path = filedialog.askopenfilename(title='Open Image File', filetypes=types)
        if len(path) == 0:
            return
            
        self.__loadingImage(path)

    def __actionComboboxTypeG(self, event):
        """
        Type:
            (0) Line
            (1) Fill
        :param event: combobox event
        """
        index = self.__comboboxTypeG.current()
        if index == -1 or self.__option == index:
            return
        
        self.__option = index
        self.__drawCanvas()

    def __drawAxes(self):
        """
        Draw coordinate axis X and Y on canvas
        """
        # cw, ch = self.__canvas.winfo_reqwidth(), self.__canvas.winfo_reqheight()
        cw, ch = self.__canvas.winfo_width(), self.__canvas.winfo_height()
        # print (cw, ch)

        x_left, x_right = 70, 10
        y_top, y_bottom = 10, 40
        self.__o = [x_left, ch - y_bottom]
        self.__px = [cw - x_right, ch - y_bottom]
        self.__py = [x_left, y_top]

        # create axis 'y'
        self.__canvas.create_line(self.__py[0], self.__py[1], self.__o[0], self.__o[1], arrow=FIRST, tags="axes")

        # create axis 'x'
        self.__canvas.create_line(self.__o[0], self.__o[1], self.__px[0], self.__px[1], arrow=LAST, tags="axes")
        
        del cw, ch, x_left, x_right, y_top, y_bottom

    def __createHistogramRGB(self):
        """
        Create a RGB histogram
        """
        # if hasattr(self, '__histogram'):
        #    return

        self.__histogram = [0 for i in range(256*3)]
        # iw, ih = self.__fn.width(), self.__fn.height()
        (iw, ih) = self.__fn.size 
        for i in range(iw):
            for j in range(ih):
                pixel = self.__fn.getpixel( (i, j) ) 
                # pixel = (120, 120, 120)
                # pixel = string.split( self.__fn.get(int(i), int(j)) )
                r, g, b = pixel[0], pixel[1], pixel[2]
                self.__histogram[r] += 1
                self.__histogram[g + 256] += 1
                self.__histogram[b + 512] += 1
                
        del r, g, b, pixel

    def __drawHistogram(self):
        """
        Draw histogram on canvas
        """
        hg = self.__histogram
        maxY = max(hg)

        self.__canvas.create_text(self.__py[0] - 10, self.__py[1] + 10, text=str(maxY), anchor='e', tags="texts")
        self.__canvas.create_text(self.__o[0] - 10, self.__o[1], text='0', anchor='e', tags="texts")

        lengthX = self.__length2Points(self.__o, self.__px) - 10
        lengthY = self.__length2Points(self.__o, self.__py) - 10

        maxX = lengthX / (256 * 3)
        xa = self.__o[0] + 1
        ya = self.__o[1] - 1

        x = 0
        y = 0
        xex = xa
        it = 0
        color = 'black'

        aux_x = xa
        aux_y = ya

        for i in range(256*3):            
            x = xex
            xex = x + maxX
            y = ya - ((hg[int(i)] * lengthY) / maxY)
            if i <= 255:
                color = "#%02x%02x%02x" % (it, 0, 0)
            elif i <= 511:
                color = "#%02x%02x%02x" % (0, it, 0)
            else:
                color = "#%02x%02x%02x" % (0, 0, it)

            if self.__option == 0:
                self.__canvas.create_line(aux_x, aux_y, xex, math.trunc(y), fill=color, tags="rects")
                aux_x, aux_y = xex, math.trunc(y)
            elif self.__option == 1:
                if hg[int(i)] > 0:
                    self.__canvas.create_rectangle(x, ya, xex, math.trunc(y), fill=color, outline=color, tags="rects")
            # color
            self.__canvas.create_rectangle(x, ya + 10, xex, ya + 30, fill=color, outline=color, tags="colors")
            if it == 255 or it == 511:
                it = -1
            it += 1
                
        del maxY, xa, ya, it, lengthX, lengthY, color

    def __length2Points(self, p1, p2):
        """
        Calculate length between two points
        
        :param p1: tuple(x1, y1)
        :param p2: tuple(x2, y2)
        :return: length
        """
        return math.sqrt(math.pow(p2[0] - p1[0], 2) + math.pow(p2[1] - p1[1], 2))

    def __drawCanvas(self):
        """
        Draw all graphics
        """
        self.__clearCanvas()
        self.__drawAxes()
        self.__drawHistogram() 

    def __loadingImage(self, imagePath):
        """
        Create a thread for load image
        
        :param imagePath: image path
        """
        thread = threading.Thread(target=self.__runLoading, args=(imagePath,))
        thread.start()

    def __runLoading(self, imagePath):
        """
        Load image, create histogram and draw on canvas
        
        :param pathImage: selected image path
        """
        self.__lblmsg.configure(text="Loading image...")
        # self.__progressbar.configure(mode='indeterminate')
        self.__progressbar.start()
        # 
        self.__fn = Image.open(imagePath)
        
        self.__lblmsg.configure(text="Creating histogram...")
        self.__createHistogramRGB()
        #
        self.__lblmsg.configure(text="Drawing histogram...")
        self.__drawCanvas()
        #
        self.__progressbar.stop()
        # self.__progressbar.configure(mode='determinate')
        self.__lblmsg.configure(text="")

    def __clearCanvas(self):
        """
        Clear canvas, remove all graphics
        """
        self.__canvas.delete("texts")
        self.__canvas.delete("axes")
        self.__canvas.delete("rects")
        self.__canvas.delete("colors")

    def __resizeCanvas(self, event):
        """
        Redraw canvas, when canvas resize dimensions
        :param event: resize event
        """
        if len(self.__histogram) == 0:
            return
        self.__drawCanvas()

    def __del__(self):
        """
        Free memory
        """
        del self.__canvas, self.__histogram, self.__fn,self.__o, self.__px, self.__py
