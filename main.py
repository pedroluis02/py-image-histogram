# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 13:46:24 2013

@author: Pedro Luis
"""
from tkinter import ttk
from mainframe import MainFrame

if __name__ == "__main__":
    """ Main function """
    print("run main...")

    themes = ttk.Style().theme_names()
    print("themes:",  themes)
    if [t for t in themes if t == 'clam']:
        print("apply clam theme")
        ttk.Style().theme_use('clam')
    print("current theme: ", ttk.Style().theme_use())

    gm = MainFrame()
    gm.master.title("Image Histogram")
    gm.mainloop()
